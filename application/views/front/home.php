<main>
    <div id="carousel-home">
        <div class="owl-carousel owl-theme">

            <?php foreach ($slider as $row) { ?>


                <div class="owl-slide cover"
                     style="background-image: url(<?php echo $this->main->image_preview_url($row->thumbnail) ?>);">
                    <div class="opacity-mask d-flex align-items-center" data-opacity-mask="rgba(0, 0, 0, 0.4)">
                        <div class="container">
                            <div class="row justify-content-center justify-content-md-start">
                                <div class="col-lg-12 static">
                                    <div class="slide-text text-<?php echo $row->align ?> white">
                                        <h2 class="owl-slide-animated owl-slide-title"><?php echo $row->title ?></h2>
                                        <p class="owl-slide-animated owl-slide-subtitle">
                                            <?php echo $row->description ?>
                                        </p>
                                        <div class="owl-slide-animated owl-slide-cta">
                                            <a class="btn_1"
                                               href="<?php echo site_url('rekomendasi-toko') ?>"
                                               role="button">BELANJA SEKARANG
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            <?php } ?>

        </div>
        <div id="icon_drag_mobile"></div>
    </div>


    <div class="bg_gray">
        <div class="container margin_60_40">
            <div class="main_title center add_bottom_10">
                <span><em></em></span>
                <h2>Petunjuk Pemesanan</h2>
                <p>Ikuti Tahap - Tahap dibawah untuk melakukan pemesanan.</p>
            </div>
            <div class="row justify-content-md-center how_2">
                <div class="col-lg-5 text-center">
                    <figure>
                        <img src="data:image/gif;base64,R0lGODlhAQABAIAAAP///wAAACH5BAEAAAAALAAAAAABAAEAAAICRAEAOw=="
                             data-src="<?php echo site_url('assets/template_front/') ?>img/web_wireframe.svg" alt=""
                             class="img-fluid lazy" width="360" height="380">
                    </figure>
                </div>
                <div class="col-lg-5">
                    <ul>
                        <li>
                            <h3><span>#01.</span> Pilih Merchant</h3>
                            <p>Pilih Merchant Sesuai dengan Kategori yang Anda Inginkan</p>
                        </li>
                        <li>
                            <h3><span>#02.</span> Transaksi</h3>
                            <p>Hubungi Nomor Yang Telah Tersedia dan Transfer Ke Rekening BRI Pejual</p>
                        </li>
                        <li>
                            <h3><span>#03.</span> Nikmati</h3>
                            <p>Kirimkan Bukti Transfer dan Pesananmu Siap Diantarkan</p>
                        </li>
                    </ul>
                    <p class="add_top_30"><a href="<?php echo site_url() ?>" class="btn_1">Info Selengkapnya <i
                                    class="icon-arrow-right"></i> </a></p>
                </div>

            </div>
        </div>

    </div>

    <div class="wrap-lokasi">
        <div class="container margin_60_40">
            <div class="main_title center">
                <span><em></em></span>
                <h2>Kategori Populer</h2>
                <p>Berikut Daftar Barang yang Sering Dibeli oleh Pengunjung</p>
            </div>

            <div class="row">
                <?php foreach ($category as $row) { ?>
                    <div class="col-md-4 col-xs-12">
                        <div class="list_home">
                            <ul>
                                <li>
                                    <a href="<?php echo $this->main->permalink(array('toko', $row->title)) ?>">
                                        <figure>
                                            <img src="<?php echo $this->main->image_preview_url($row->thumbnail) ?>"
                                                 data-src="<?php echo $this->main->image_preview_url($row->thumbnail) ?>"
                                                 alt="<?php echo $row->thumbnail_alt ?>" class="lazy">
                                        </figure>
                                        <h3><?php echo $row->title ?></h3>
                                        <br/>
                                        <div class="score"><strong><?php echo number_format($row->shop_total) ?>
                                                Toko</strong></div>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                <?php } ?>
            </div>
        </div>
    </div>

    <div class="bg_gray">
        <div class="container margin_60_40">
            <div class="main_title center">
                <span><em></em></span>
                <h2>Rekomendasi Toko</h2>
                <p>Daftar Toko yang Sering dikunjungi di PasarSukawati.com</p>
            </div>
            <div class="row">
                <?php foreach($shop as $row) { ?>


                    <div class="col-xl-4 col-lg-6 col-md-6 col-sm-6 col-xs-12">
                        <div class="strip">
                            <figure>
                                <img src="<?php echo $this->main->image_preview_url($row->thumbnail) ?>"
                                     data-src="<?php echo $this->main->image_preview_url($row->thumbnail) ?>"
                                     class="img-fluid lazy" alt="<?php echo $row->thumbnail_alt ?>">
                                <a href="https://wa.me/<?php echo $row->whatsapp ?>" class="strip_info" target="_blank">
                                    <div class="item_title">
                                        <h3><?php echo $row->title ?></h3>
                                        <small><?php echo $row->category_title ?></small>
                                        <Br />
                                        <div class="float-left">
                                            <img src="<?php echo base_url('assets/template_front/img/logo-whatsapp.png') ?>" width="32">
                                        </div>
                                        <div class="float-left" style="padding-left: 10px;">
                                            <div class="chat-penjual">Klik Chat Penjual</div>
                                            <div class="chat-penjual"><?php echo $row->whatsapp ?></div>
                                        </div>
                                    </div>
                                </a>
                            </figure>
                        </div>
                    </div>

                <?php } ?>
            </div>

            <p class="text-center add_top_30"><a href="<?php echo site_url('rekomendasi-toko') ?>" class="btn_1 medium">Lihat Toko
                    Lain</a></p>
        </div>

    </div>

    <div class="wrap-lokasi">
        <div class="container margin_60_40">
            <div class="row">
                <div class="col-md-6">
                    <div class="row">
                        <div class="col-12">
                            <h2 class="mb-3">
                                PASAR SUKAWATI </h2>
                            <hr/>
                        </div>
                        <div class="col-md-6">
                            <h3 class="text-uppercase mt-3">Lokasi</h3>
                            <p>Jalan Raya Sukawati, Sukawati, Kec. Sukawati, Kabupaten Gianyar, Bali 80582</p>
                        </div>
                        <div class="col-md-6">
                            <h3 class="text-uppercase mt-3">Pengaduan</h3>
                            <p>Anda kurang puas atau ada keluhan saat belanja ? Hubungi Pengelola Pasar Sukawati <a href="telp:0361290132">0361 290132</a></p>
                        </div>
                        <div class="col-md-6">
                            <h3 class="text-uppercase mt-3">Jam Buka</h3>
                            <p>
                                SETIAP HARI :<br>
                                PUKUL 06:00 – 13:00 WITA<br>
                            </p>
                        </div>
                        <div class="col-md-6">
                            <h3 class="text-uppercase mt-3">Waktu Pengantaran</h3>
                            <p>
                                SETIAP HARI :<br>
                                PUKUL 07:00 - 13:00 WITA<br>
                            </p>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3944.9944419837375!2d115.28041681527742!3d-8.596530693821599!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2dd23e28616c410d%3A0x960eaffe9f543e3c!2sPasar%20Seni%20Sukawati!5e0!3m2!1sid!2sid!4v1592037344853!5m2!1sid!2sid"
                            width="100%" height="400" frameborder="0" style="border:0;" allowfullscreen=""
                            aria-hidden="false" tabindex="0"></iframe>
                </div>
            </div>
        </div>
    </div>
</main>